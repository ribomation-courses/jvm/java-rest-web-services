package mn.persons;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.*;
import java.util.List;
import java.util.Optional;

@Controller("/persons")
public class PersonsController {
    private final PersonRepo personRepo;

    public PersonsController(PersonRepo personRepo) {
        this.personRepo = personRepo;
    }

    @Get("/")
    public List<Person> all() {
        return personRepo.findAll();
    }

    @Post("/")
    @Status(HttpStatus.CREATED)
    public Person create(@Body Person p) {
        return personRepo.insert(p.getName(), p.getAge());
    }

    @Get("/{id}")
    public Optional<Person> one(int id) {
        return personRepo.findById(id);
    }

    @Put("/{id}")
    public Optional<Person> update(int id, @Body Person p) {
        return personRepo.update(id, p.getName(), p.getAge());
    }

    @Delete("/{id}")
    public HttpResponse remove(int id) {
        if (personRepo.findById(id).isEmpty()) return HttpResponse.notFound();
        personRepo.remove(id);
        return HttpResponse.noContent();
    }
}
