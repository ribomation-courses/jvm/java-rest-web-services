package ribomation.java_rest_ws.json;

import com.goebl.david.Webb;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonClient_DavidWebb {
    static final String uriAll = "http://localhost:8888/persons";
    static final String uriOne = "http://localhost:8888/person/";

    public static void main(String[] args) throws Exception {
        new JsonClient_DavidWebb().run();
    }

    void run() throws Exception {
        all();
        one(3);
        int id = create("Inge Vidare", 39);
        all();
        update(id, 43);
        remove(id);
        try {
            one(id);
        } catch (Exception e) {
            System.out.printf("ERR: %s%n", e);
        }
        all();
    }

    void all() throws JSONException {
        System.out.printf("--- ALL%n");
        JSONArray list = Webb.create()
                .get(uriAll)
                .ensureSuccess()
                .asJsonArray()
                .getBody();
        System.out.printf("JSON Response:%n%s%n",
                list.join("\n"));
    }

    void one(int id) throws JSONException {
        System.out.printf("--- ONE%n");
        JSONObject obj = Webb.create()
                .get(uriOne + id)
                .ensureSuccess()
                .asJsonObject()
                .getBody();
        System.out.printf("JSON Response:%n%s%n",
                obj.toString(2));
    }

    int create(String name, int age) throws JSONException {
        System.out.printf("--- CREATE%n");
        JSONObject obj = Webb.create()
                .post(uriAll)
                .body(new JSONObject()
                        .put("name", name)
                        .put("age", age))
                .ensureSuccess()
                .asJsonObject()
                .getBody();
        System.out.printf("JSON Response:%n%s%n",
                obj.toString());
        return obj.getInt("id");
    }

    void update(int id, int age) throws JSONException {
        System.out.printf("--- UPDATE%n");
        JSONObject obj = Webb.create()
                .put(uriOne + id)
                .body(new JSONObject().put("age", age))
                .ensureSuccess()
                .asJsonObject()
                .getBody();
        System.out.printf("JSON Response:%n%s%n",
                obj.toString());
    }

    void remove(int id) {
        System.out.printf("--- DELETE%n");
        Webb.create().delete(uriOne + id)
                .ensureSuccess()
                .asVoid();
    }

}
