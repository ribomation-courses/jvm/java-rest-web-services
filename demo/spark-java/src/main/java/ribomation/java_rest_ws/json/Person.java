package ribomation.java_rest_ws.json;

import java.util.Objects;

public class Person {
    static int nextId = 1;
    int    id = -1;
    String name;
    int    age;

    Person() {
    }

    Person(String name, int age) {
        this(nextId++, name, age);
    }

    Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    Person assignId() {
        id = nextId++;
        return this;
    }

    @Override
    public String toString() {
        return String.format("Person{ID=%d, name=%s, age=%d}", id, name, age);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id &&
                age == person.age &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age);
    }
}
