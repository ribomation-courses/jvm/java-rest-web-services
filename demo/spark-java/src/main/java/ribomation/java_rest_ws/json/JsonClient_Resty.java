package ribomation.java_rest_ws.json;


import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;
import us.monoid.web.Resty;

import java.io.IOException;

public class JsonClient_Resty {
    static final String uriAll = "http://localhost:8888/persons";
    static final String uriOne = "http://localhost:8888/person/";

    public static void main(String[] args) throws Exception {
        new JsonClient_Resty().run();
    }

    void run() throws Exception {
        all();
        one(1);
        int id = create("Inge Vidare", 23);
        all();
        update(id, 33);
        remove(id);
        try {
            one(id);
        } catch (Exception e) {
            System.out.printf("ERR: %s%n", e);
        }
        all();
    }

    void all() throws IOException, JSONException {
        System.out.printf("--- ALL%n");
        JSONArray list = new Resty()
                .json(uriAll)
                .array();
        System.out.printf("JSON Response:%n%s%n",
                list.join("\n"));
    }

    void one(int id) throws IOException, JSONException {
        System.out.printf("--- ONE%n");
        JSONObject obj = new Resty()
                .json(uriOne + id)
                .object();
        System.out.printf("JSON Response:%n%s%n",
                obj.toString(2));
    }

    int create(String name, int age) throws IOException, JSONException {
        System.out.printf("--- CREATE%n");
        JSONObject obj2 = new Resty()
                .json(uriAll,
                        Resty.content(new JSONObject()
                                .put("name", name)
                                .put("age", age)))
                .object();
        System.out.printf("JSON Response:%n%s%n", obj2.toString());
        return obj2.getInt("id");
    }

    void update(int id, int age) throws JSONException, IOException {
        System.out.printf("--- UPDATE%n");
        JSONObject obj = new Resty()
                .json(uriOne + id,
                        Resty.put(
                                Resty.content(new JSONObject()
                                        .put("age", age))))
                .object();
        System.out.printf("JSON Response:%n%s%n",
                obj.toString());
    }

    void remove(int id) throws IOException {
        System.out.printf("--- DELETE%n");
        new Resty()
                .json(uriOne + id, Resty.delete());
    }

}
