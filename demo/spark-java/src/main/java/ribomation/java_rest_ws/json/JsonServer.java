package ribomation.java_rest_ws.json;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static spark.Spark.*;

public class JsonServer {
    public static void main(String[] args) {
        port(8888);

        final Logger LOG = LoggerFactory.getLogger(JsonServer.class);
        notFound((req, res) -> {
            res.status(404);
            return null;
        });
        exception(IndexOutOfBoundsException.class, (ex, req, res) -> {
            LOG.warn("ERR: {}", ex.toString());
            res.status(404);
        });
        exception(NumberFormatException.class, (ex, req, res) -> {
            LOG.warn("ERR: {}", ex.toString());
            res.status(404);
        });

        final String JSON = "application/json";
        final Gson   gson = new Gson();
        after((req, res) -> {
            res.type(JSON);
        });

        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Anna Conda", 42));
        persons.add(new Person("Per Silja", 32));
        persons.add(new Person("Justin Time", 27));
        persons.add(new Person("Sham Poo", 51));
        persons.forEach(System.out::println);

        get("/persons", (req, res) -> persons, gson::toJson);

        post("/persons", JSON, (req, res) -> {
            LOG.info("POST");
            Person p = gson.fromJson(req.body(), Person.class);
            if (p == null) halt(400);

            persons.add(p.assignId());
            LOG.info("POST {}", p);

            res.status(201);
            return p;
        }, gson::toJson);

        get("/person/:id", (req, res) -> {
            LOG.info("GET id");
            Optional<Person> P = find(req, persons);
            if (P.isPresent()) {
                LOG.info("GET {}", P.get());
                return P.get();
            } else {
                halt(404);
                return null;
            }
        }, gson::toJson);

        put("/person/:id", "application/json", (req, res) -> {
            LOG.info("PUT");
            Optional<Person> P = find(req, persons);
            if (P.isPresent()) {
                Person p = P.get();
                LOG.info("PUT {}", p);
                Person p2 = gson.fromJson(req.body(), Person.class);
                if (p2.name != null) p.name = p2.name;
                if (p2.age > 0) p.age = p2.age;
                return p;
            } else {
                halt(404);
                return null;
            }
        }, gson::toJson);

        delete("/person/:id", "application/json", (req, res) -> {
            find(req, persons)
                    .ifPresentOrElse(p -> {
                        LOG.info("DELETE {}", p);
                        persons.remove(p);
                    }, () -> halt(404));
            res.status(204);
            return null;
        }, gson::toJson);
    }

    static Optional<Person> find(Request r, List<Person> persons) {
        int id = Integer.parseInt(r.params("id"));
        return persons.stream()
                .filter(p -> p.id == id)
                .findFirst();
    }

}
