package ribomation.java_rest_ws.books;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

public class BookRestClient {
    String HOST     = "localhost";
    int    PORT     = 8000;
    String RESOURCE = "books";

    public static void main(String[] args) throws Exception {
        new BookRestClient().run();
    }

    void run() throws Exception {
        all();

        int id = createBook(new Book("Java REST WS", "Jens Riboe", 542));
        all();

        Book book = getBook(id);
        updateBook(book);

        removeBook(book);
        try {
            getBook(id);
        } catch (IOException e) {
            System.out.printf("ERR: %s%n", e);
        }
        all();
    }

    void all() throws IOException {
        System.out.printf("%n**** Fetch all books ***%n");
        list().stream().map(Book::toString).forEach(System.out::println);
    }

    int createBook(Book book) throws IOException {
        System.out.printf("%n**** Create new book ***%n");
        book = create(book);
        System.out.printf("Created: %s%n", book);
        return book.getId();
    }

    Book getBook(int id) throws IOException {
        System.out.printf("%n**** Fetch one book ***%n");
        Book book = get(id);
        System.out.printf("Retrieved: %s%n", book);
        return book;
    }

    void updateBook(Book book) throws IOException {
        System.out.printf("%n**** Update one book ***%n");
        book.setTitle("Java 8 REST Web Services");
        book.setPrice(600);
        book = replace(book);
        System.out.printf("Updated: %s%n", book);
    }

    void removeBook(Book book) throws IOException {
        System.out.printf("%n**** Remove one book ***%n");
        remove(book.getId());
        System.out.printf("Removed: %s%n", book);
    }

    List<Book> list() throws IOException {
        HttpURLConnection con = open(resource(), "GET");
        BufferedReader    in  = new BufferedReader(new InputStreamReader(con.getInputStream()));
        return in.lines().map(Book::fromCSV).collect(Collectors.toList());
    }

    Book get(int id) throws IOException {
        HttpURLConnection con = open(resource(id), "GET");
        return recv(con.getInputStream());
    }

    Book create(Book book) throws IOException {
        HttpURLConnection con = open(resource(), "POST");
        send(book, con.getOutputStream());
        return recv(con.getInputStream());
    }

    Book replace(Book book) throws IOException {
        HttpURLConnection con = open(resource(book.getId()), "PUT");
        send(book, con.getOutputStream());
        return recv(con.getInputStream());
    }

    void remove(int id) throws IOException {
        HttpURLConnection con  = open(resource(id), "DELETE");
        int               code = con.getResponseCode();
        if (code != 204) {
            throw new RuntimeException("remove failed: HTTP code=" + code);
        }
    }

    void send(Book book, OutputStream os) throws IOException {
        OutputStreamWriter w = new OutputStreamWriter(os, "US-ASCII");
        w.write(book.toCSV());
        w.flush();
        w.close();
    }

    Book recv(InputStream is) {
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        return in.lines().map(Book::fromCSV).findFirst().orElse(null);
    }

    HttpURLConnection open(URL url, String op) throws IOException {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(op);
        con.setDoOutput(!op.equals("GET") || !op.equals("DELETE"));
        con.setDoInput(true);
        con.setConnectTimeout(10 * 1000);
        con.setRequestProperty("Accept", "*/*");
        con.setInstanceFollowRedirects(true);
        con.connect();
        return con;
    }

    URL resource() throws MalformedURLException {
        return resource(HOST, PORT, RESOURCE, -1);
    }

    URL resource(int id) throws MalformedURLException {
        return resource(HOST, PORT, RESOURCE, id);
    }

    URL resource(String host, int port, String resource, int id) throws MalformedURLException {
        if (id >= 0) {
            return new URL(String.format("http://%s:%d/%s/%d", host, port, resource, id));
        }
        return new URL(String.format("http://%s:%d/%s", host, port, resource));
    }

}
