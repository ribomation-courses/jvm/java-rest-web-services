package ribomation.java_rest_ws.books;

import java.io.Serializable;
import java.util.Locale;

public class Book implements Serializable {
    private int    id;
    private String title;
    private String author;
    private float  price;

    public Book() { }

    public Book(String title, String author, float price) {
        this(-1, title, author, price);
    }

    public Book(int id, String title, String author, float price) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.price = price;
    }

    public static Book fromCSV(String csv) {
        return fromCSV(csv, ";");
    }

    public static Book fromCSV(String csv, String delim) {
        String[] fields = csv.split(delim);
        int      idx    = 0;
        int      id     = Integer.parseInt(fields[idx++]);
        String   title  = fields[idx++];
        String   author = fields[idx++];
        float    price  = Float.parseFloat(fields[idx]);
        return new Book(id, title, author, price);
    }

    public String toCSV() {
        return toCSV(";");
    }

    public String toCSV(String delim) {
        return String.join(delim,
                Integer.toString(getId()), getTitle(), getAuthor(), Float.toString(getPrice()));
    }

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH,
                "Book{id=%d, title=%s, author=%s, price=%,.2f}",
                getId(), getTitle(), getAuthor(), getPrice());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (id != book.id) return false;
        if (Float.compare(book.price, price) != 0) return false;
        if (title != null ? !title.equals(book.title) : book.title != null) return false;
        return author != null ? author.equals(book.author) : book.author == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (price != +0.0f ? Float.floatToIntBits(price) : 0);
        return result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
