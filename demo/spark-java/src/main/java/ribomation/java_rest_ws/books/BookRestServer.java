package ribomation.java_rest_ws.books;

import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static spark.Spark.*;

public class BookRestServer {
    public static void main(String[] args) {
        int port = args.length == 0 ? 8000 : Integer.parseInt(args[0]);
        new BookRestServer().run(port);
    }

    void run(int port) {
        books = populateBooks();
        System.out.printf("Books: %s%n", books);

        configure(port);
        routes();

        System.out.printf("REST Server started%n");
        System.out.printf("http://%s:%d/%s/ID%n", "localhost", port, "books");
    }

    Map<Integer, Book> books;

    Map<Integer, Book> populateBooks() {
        String[] data = {
                "Richard III;William Shakespeare",
                "The Three Musketeers;Alexandre Dumas",
                "Seven languages in seven weeks;Bruce A. Tate",
                "The Pragmatic Programmer;Andrew Hunt, David Thomas",
                "Concurrent Programming in Java, 2nd ed;Dough Lea",
                "Core Java, Vol I;Cay S. Horstmann, Gary Cornell"
        };

        AtomicInteger id = new AtomicInteger(1);
        Random        r  = new Random();
        return Stream.of(data)
                .map(d -> d.split(";"))
                .map(d -> new Book(id.getAndIncrement(), d[0], d[1], r.nextInt(1000)))
                .collect(Collectors.toConcurrentMap(Book::getId, Function.identity()))
                ;
    }

    void configure(int port) {
        port(port);

        after((req, res) -> {
            res.header("Content-Type", "text/plain");
        });

        exception(Exception.class, (err, req, res) -> {
            err.printStackTrace();
            res.status(500);
            res.body(err.toString());
        });
    }

    void routes() {
        final String EOL = "\r\n";

        get("/books", (request, response) -> {
            if (books.isEmpty()) {
                response.status(204);
                return "";
            }

            List<String> booksCSV = new ArrayList<>();
            for (Book book : books.values()) booksCSV.add(book.toCSV());
            return String.join(EOL, booksCSV) + EOL;
        });

        post("/books", (request, response) -> {
            int  id   = books.isEmpty() ? 0 : books.keySet().stream().max(Integer::compare).orElse(0);
            Book book = Book.fromCSV(request.body());
            book.setId(++id);
            books.put(id, book);

            response.status(201);
            return books.get(id).toCSV();
        });

        get("/books/:id", (request, response) -> {
            int id = lookup(request, response);
            if (id < 0) return "";

            return books.get(id).toCSV();
        });

        put("/books/:id", (request, response) -> {
            int id = lookup(request, response);
            if (id < 0) return "";

            Book book = Book.fromCSV(request.body());
            books.put(id, book);

            return books.get(id).toCSV();
        });

        delete("/books/:id", (request, response) -> {
            int id = lookup(request, response);
            if (id < 0) return "";

            books.remove(id);

            response.status(204);
            return "";
        });
    }

    int lookup(Request req, Response res) {
        int id = Integer.parseInt(req.params(":id"));
        if (!books.containsKey(id)) {
            res.status(404);
            return -1;
        }
        return id;
    }
}
