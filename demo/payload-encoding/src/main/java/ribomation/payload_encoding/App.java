package ribomation.payload_encoding;

import org.apache.commons.io.HexDump;
import ribomation.payload_encoding.codecs.*;
import ribomation.payload_encoding.domain.DataFactory;
import ribomation.payload_encoding.domain.Person;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class App {
    public static void main(String[] args) {
        new App().run();
    }

    void run() {
        Codec[] codecs = {
                new XML(),
                new JSON(),
                new CSV(),
                new FWV(),
                new Hessian()
        };

        var persons = new DataFactory().mkPersons(100);
        persons.forEach(System.out::println);

        var results = run(persons, codecs);
        printSummary(results);
    }

    Map<Codec, Integer> run(List<Person> target, Codec[] codecs) {
        Map<Codec, Integer> results = new LinkedHashMap<>();
        for (Codec codec : codecs) {
            int size = run(target, codec, codec.isText());
            results.put(codec, size);
        }
        return results;
    }

    void printSummary(Map<Codec, Integer> results) {
        System.out.printf("--- Payload Size ---%n");
        var minSize = results.values().stream().min(Integer::compareTo).orElse(1);
        results.forEach((codec, size) ->
                System.out.printf("%-7s: %6d bytes %6.0f%% %n",
                        codec.getClass().getSimpleName(),
                        size,
                        100.0 * size / minSize
                ));
    }

    int run(List<Person> target, Codec codec, boolean text) {
        System.out.printf("--- %s ---%n", codec.getClass().getSimpleName());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        codec.encode(target, out);
        byte[] payload = out.toByteArray();

        if (text) {
            System.out.println(new String(payload));
        } else {
            System.out.println(toHEX(payload));
            System.out.println();
        }

        ByteArrayInputStream in       = new ByteArrayInputStream(payload);
        List<Person>         restored = codec.decode(in);
        if (notEquals(target, restored)) {
            System.out.println("*** Restored list =/= target");
            restored.forEach(System.out::println);
        }

        return payload.length;
    }

    boolean notEquals(List<Person> lst1, List<Person> lst2) {
        if (lst1 == null || lst2 == null) {
            System.out.printf("*** null lists: %n");
            return true;
        }

        if (lst1.size() != lst2.size()) {
            System.out.printf("*** size differ: %d =/= %d%n", lst1.size(), lst2.size());
            return true;
        }

        for (int k = 0; k < lst1.size(); ++k) {
            Person p1 = lst1.get(k);
            Person p2 = lst2.get(k);
            if (!p1.equals(p2)) {
                System.out.printf("*** %s%n=/= %s%n", p1, p2);
                return true;
            }
        }

        return false;
    }

    String toHEX(byte[] data) {
        var buf = new ByteArrayOutputStream();
        try {
            HexDump.dump(data, 0, buf, 0);
            return buf.toString();
        } catch (IOException e) {
            return e.getMessage();
        }
    }
}
