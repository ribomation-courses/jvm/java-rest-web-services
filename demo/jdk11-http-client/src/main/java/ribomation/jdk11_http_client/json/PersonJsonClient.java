package ribomation.jdk11_http_client.json;

import java.io.IOException;

public class PersonJsonClient {
    public static void main(String[] args) throws Exception {
        PersonJsonClient app = new PersonJsonClient();
        app.run();
    }

    void run() throws IOException, InterruptedException {
        JsonCRUD<Person> crud = new JsonCRUD<>("http://localhost:8888", "person", Person.class);

        var obj = crud.read(1);
        System.out.printf("obj: %s%n", obj);

        obj = crud.create(new Person("Nisse", 42));
        System.out.printf("obj: %s%n", obj);

        final int id = obj.getId();
        obj = crud.update(id, new Person("Berra", obj.getAge()));
        System.out.printf("obj: %s%n", obj);

        var rc = crud.delete(id);
        System.out.printf("rc = %d%n", rc);

        var all = crud.readAll(Person.class);
        System.out.println("all = " + all);
    }
}
