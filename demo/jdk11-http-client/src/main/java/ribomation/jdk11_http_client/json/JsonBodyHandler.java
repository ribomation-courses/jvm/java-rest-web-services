package ribomation.jdk11_http_client.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.net.http.HttpResponse;
import java.util.function.Function;

public class JsonBodyHandler<T> implements HttpResponse.BodyHandler<T> {
    private Class<T> type;
    private Gson     gson;

    public JsonBodyHandler(Class<T> type) {
        this.type = type;
        this.gson = new GsonBuilder().setPrettyPrinting().create();
    }

    @Override
    public HttpResponse.BodySubscriber<T> apply(HttpResponse.ResponseInfo info) {
        System.out.printf("RES: status=%s%n", info.statusCode());
        info.headers().map().forEach((name, val) -> System.out.printf("  %s: %s%n", name, val));

        Function<byte[], T> mapper = (byte[] payload) ->
                gson.fromJson(new InputStreamReader(new ByteArrayInputStream(payload)), type);

        HttpResponse.BodySubscriber<byte[]> upstream =
                HttpResponse.BodySubscribers.ofByteArray();

        return HttpResponse.BodySubscribers.mapping(upstream, mapper);
    }
}


