package ribomation.jdk11_http_client.simple;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

public class SimpleGetRequest {
    public static void main(String[] args) throws Exception {
        SimpleGetRequest app = new SimpleGetRequest();
        app.run();
    }

    void run() throws IOException, InterruptedException {
        var req = HttpRequest.newBuilder()
                .uri(URI.create("https://www.ribomation.se/"))
                .GET()
                .build();

        var client = HttpClient.newBuilder()
                .followRedirects(HttpClient.Redirect.NORMAL)
                .connectTimeout(Duration.ofSeconds(5))
                .build();

        var res = client.send(req, HttpResponse.BodyHandlers.ofLines());
        res.body()
                .limit(12)
                .forEach(System.out::println);
    }
}


