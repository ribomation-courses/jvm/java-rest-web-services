const express   = require('express')
const swaggerUI = require('swagger-ui-dist');

const port = 3000;
const app  = express();
app.use(express.static(swaggerUI.absolutePath()));
app.listen(port);
console.log(`Swagger UI running: http://localhost:3000/`);

