package products.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Product {
    private int    id;
    private String name;
    private float  price;
    private int    itemsInStock;

    @JsonFormat(pattern = "yyyy-MM-dd",shape = JsonFormat.Shape.STRING)
    private Date   modificationDate;

    public Product() {
    }

    public Product(String id, String name, String price, String itemsInStock, String modificationDate) {
        this(
                Integer.parseInt(id),
                name,
                Float.parseFloat(price),
                Integer.parseInt(itemsInStock),
                parseDate(modificationDate)
        );
    }

    public Product(int id, String name, float price, int itemsInStock, Date modificationDate) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.itemsInStock = itemsInStock;
        this.modificationDate = modificationDate;
    }

    public static Product fromCSV(String csv) {
        return fromCSV(csv, ",");
    }

    public static Product fromCSV(String csv, String delim) {
        //id,name,price,itemsInStock,modificationDate
        //3,Grand Marquis,314.08,8,2018-11-17
        var fields = csv.split(delim);
        return new Product(fields[0], fields[1], fields[2], fields[3], fields[4]);
    }

    private static Date parseDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (Exception e) {
            System.out.printf("*** ERR: %s%n", e);
        }
        return new Date();
    }

    public Product update(Product p) {
        if (p.name != null) {
            this.name = p.name;
        }
        if (p.price > 0) {
            this.price = p.price;
        }
        if (p.itemsInStock > -1) {
            this.itemsInStock = p.itemsInStock;
        }
        if (p.modificationDate != null) {
            this.modificationDate = p.modificationDate;
        }
        return this;
    }

    @Override
    public String toString() {
        return String.format("Product{name=%s, price=%.2f, count=%d, date=%tF",
                name, price, itemsInStock, modificationDate
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id &&
                Float.compare(product.price, price) == 0 &&
                itemsInStock == product.itemsInStock &&
                name.equals(product.name) &&
                modificationDate.equals(product.modificationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, itemsInStock, modificationDate);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getItemsInStock() {
        return itemsInStock;
    }

    public void setItemsInStock(int itemsInStock) {
        this.itemsInStock = itemsInStock;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }
}
