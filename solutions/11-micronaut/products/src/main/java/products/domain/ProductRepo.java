package products.domain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProductRepo {
    public static final ProductRepo instance = new ProductRepo("/products.csv");
    private final Map<Integer, Product> products;

    private ProductRepo(String path) {
        products = load(path);
    }

    private Map<Integer, Product> load(String path)  {
        var is = getClass().getResourceAsStream(path);
        if (is == null) {
            throw new IllegalArgumentException("Cannot open " + path);
        }
        var in = new BufferedReader(new InputStreamReader(is));
        try (in) {
            return in.lines()
                    .skip(1)
                    .map(Product::fromCSV)
                    .collect(Collectors.toMap(p -> p.getId(), p -> p))
                    ;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Collection<Product> findAll() {
        return products.values();
    }

    public Optional<Product> findById(int id) {
        return Optional.ofNullable(products.get(id));
    }

    public void delete(int id) {
        products.remove(id);
    }

    public Product insert(Product p) {
        var max = products.keySet().stream().mapToInt(id -> id).max();
        p.setId(max.orElse(0) + 1);
        products.put(p.getId(), p);
        return p;
    }

}
