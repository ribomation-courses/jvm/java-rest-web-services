package products;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import products.domain.Product;
import products.domain.ProductRepo;

import java.util.Collection;

@Controller("/products")
public class ProductsController {

    @Get("/")
    public Collection<Product> findAll() {
        return ProductRepo.instance.findAll();
    }

    @Post("/")
    public Product create(Product p) {
        return ProductRepo.instance.insert(p);
    }

    @Get("/{id}")
    public Product findOne(int id) {
        return ProductRepo.instance.findById(id).orElse(null);
    }

    @Put("/{id}")
    public Product update(int id, Product delta) {
        var maybe = ProductRepo.instance.findById(id);
        maybe.ifPresent(p -> p.update(delta));
        return maybe.orElse(null);
    }

    @Delete("/{id}")
    public void delete(int id) {
        ProductRepo.instance.delete(id);
    }

}
