# Micronaut Demo - Products

## Preparation

    $ sdk install micronaut
    $ mn --version

## Build
Use `GIT Bash` for all commands

    $ ./gradlew build

Two JAR files how now been built to `build/libs/*.jar`

## Run

    $ java -jar build/libs/products-1-all.jar

The MN server will now be listening on port 8080.

## Manual Test
Use HTTPie

    $ http -v get :8080/products
    $ http -v get :8080/products/1
    $ http -v put :8080/products/1 name=TjollaHopp
    $ http -v delete :8080/products/1
    $ http -v post :8080/products name=Apple price=25 itemsInStock=10

