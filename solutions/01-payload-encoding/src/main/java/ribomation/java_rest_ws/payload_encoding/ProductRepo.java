package ribomation.java_rest_ws.payload_encoding;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ProductRepo {
    private final List<Product> products;

    public ProductRepo() {
        try {
            products = load("/products.csv");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Product> getProducts() {
        return products;
    }

    private List<Product> load(String resourcePath) throws IOException {
        InputStream is = getClass().getResourceAsStream(resourcePath);
        if (is == null) {
            throw new IllegalArgumentException("cannot open " + resourcePath);
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        try (in) {
            return in.lines()
                    .skip(1)
                    .map(csv -> csv.split(","))
                    .map(fields -> {
                        //3,Grand Marquis,314.08,8,2018-11-17
                        int    id    = Integer.parseInt(fields[0]);
                        String name  = fields[1];
                        float  price = Float.parseFloat(fields[2]);
                        int    count = Integer.parseInt(fields[3]);
                        Date   date  = parseDate(fields[4]);
                        return new Product(id, name, price, count, date);
                    })
                    .collect(Collectors.toList())
                    ;
        }
    }

    private Date parseDate(String dateTxt) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(dateTxt);
        } catch (Exception e) {
            System.out.printf("*** ERR: %s%n", e);
        }
        return new Date();
    }
}
