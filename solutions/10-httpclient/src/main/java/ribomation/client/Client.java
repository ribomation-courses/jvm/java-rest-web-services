package ribomation.client;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Client {
    public static void main(String[] args) throws Exception {
        var baseUri = "http://localhost:5000/";
        var client  = new Client();
        client.getProducts(URI.create(baseUri + "products"));
        System.out.println("------");
        client.getProducts(URI.create(baseUri + "products/1"));
    }

    void getProducts(URI uri) throws Exception {
        var req = HttpRequest
                .newBuilder(uri)
                .GET()
                .build();
        HttpClient
                .newHttpClient()
                .send(req, HttpResponse.BodyHandlers.ofLines())
                .body()
                .forEach(System.out::println);
    }
}
