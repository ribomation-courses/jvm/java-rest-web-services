package ribomation.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import spark.Request;

import java.util.function.Function;

import static spark.Spark.*;

public class Server {
    public static void main(String[] args) {
        var port = 5000;
        var app  = new Server();
        app.setup();
        app.init(port);
        app.routes();
        System.out.printf("Server started: http://localhost:%d/products%n", port);
    }

    final String JSON = "application/json";
    ProductRepo repo;
    Gson        gson;

    void setup() {
        repo = new ProductRepo();
        repo.getProducts().forEach(System.out::println);
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd")
                .create();
    }

    void init(int port) {
        port(port);

        notFound((req, res) -> "Oooops: " + req.url());

        before((req, res) -> {
            System.out.printf("REQ: %s %s%n", req.requestMethod(), req.uri());
        });
    }

    void routes() {
        path("/products", () -> {
            get("", (req, res) -> repo.getProducts(), gson::toJson);

            post("", JSON, (req, res) -> {
                var p = gson.fromJson(req.body(), Product.class);
                if (p == null) {
                    halt(400);
                }
                res.status(201);
                return repo.insert(p);
            }, gson::toJson);

            Function<Request, Product> lookup = (req) -> {
                var id = Integer.parseInt(req.params("id"));
                var p  = repo.getProductById(id);
                if (p.isEmpty()) halt(404);
                return p.get();
            };

            get("/:id", (req, res) -> {
                return lookup.apply(req);
            }, gson::toJson);

            put("/:id", JSON, (req, res) -> {
                var p = lookup.apply(req);
                var P = gson.fromJson(req.body(), Product.class);
                if (P == null) halt(400);
                return p.update(P);
            }, gson::toJson);

            delete("/:id", (req, res) -> {
                var p = lookup.apply(req);
                repo.remove(p.getId());
                res.status(204);
                return null;
            }, gson::toJson);
        });
    }

}
