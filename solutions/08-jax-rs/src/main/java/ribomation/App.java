package ribomation;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import ribomation.domain.BooksRepo;

import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App {
    public static void main(String[] args) throws Exception {
        new App().run();
    }

    void run() throws Exception {
        Logger.getGlobal().setLevel(Level.ALL);
        URI            baseUri = URI.create("http://localhost:9000/");

        ResourceConfig cfg     = jerseyInit();
        startServer(cfg, baseUri);
        BooksRepo.instance.list().forEach(System.out::println);

        Logger         log     = Logger.getLogger(App.class.getName());
        log.info(String.format("Started: %sbooks%n", baseUri));

        Thread.currentThread().join();
    }

    ResourceConfig jerseyInit() {
        return new ResourceConfig()
                .packages(App.class.getPackage().getName())
                .register(MoxyJsonFeature.class)
                .register(new CORSFilter())
                ;
    }

    void startServer(ResourceConfig cfg, URI uri) {
        HttpServer     server  = GrizzlyHttpServerFactory.createHttpServer(uri, cfg, true);
        Runtime.getRuntime().addShutdownHook(new Thread(server::shutdownNow));
    }

}
