function populate(product) {
    $('#product-id').text(product.id);
    $('#product-name').text(product.name);
    $('#product-price').text(product.price);
    $('#product-items').text(product.itemsInStock);
    $('#product-modified').text(product.modificationDate);

    $('#product-json').html(JSON.stringify(product, null, 2).replace('\n', '\r\n'));
}

function getProduct(id) {
    console.info('HTTP GET: id:', id);

    $.get(`/products/${id}`)
        .then((payload) => {
            const product = JSON.parse(payload);
            console.info('product', product);
            populate(product);
            $('#product').show();
        })
        .fail((err) => {
            console.info('FAILURE', err);
            $('#product').hide();
            $('#product-json').text(`FAILURE: ${err.statusText}. ID=${id}`);
        });
}


$(() => {
    $('#product').hide();

    $('#fetch').click(() => {
        $('#product').hide();
        $('#product-json').text('LOADING...');
        getProduct($('#id').val());
    });

    $('#id').change(() => {
        getProduct($('#id').val());
    });

    $('#create').click(() => {
        $('#product').hide();
        $('#product-json').text('SUBMITTING...');

        const data = $('#newProduct')
            .serializeArray()
            .reduce((data, e) => {
                data[e.name] = e.value;
                return data;
            }, {});
        data.modificationDate = new Date().toLocaleDateString();
        console.info('new-product', data);

        $.post('/products', JSON.stringify(data))
            .then((payload) => {
                const product = JSON.parse(payload);
                console.info('product', product);
                populate(product);
                $('#id').val(product.id);
                $('#product').show();
            })
            .fail((err) => {
                console.info('FAILURE', err);
                $('#product-json').text(`FAILURE: ${err.statusText}`);
            })
        ;
    });
});
