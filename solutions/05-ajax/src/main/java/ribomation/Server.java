package ribomation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import spark.Request;

import java.util.function.Function;

import static spark.Spark.*;

public class Server {
    public static void main(String[] args) {
        var port = 8000;
        var srv  = new Server();
        srv.setup();
        srv.init(port);
        srv.routes();
        System.out.printf("Server started. http://localhost:%d/%n", port);
    }

    private final String      JSON = "application/json";
    private       ProductRepo repo;
    private       Gson        gson;

    private void setup() {
        repo = new ProductRepo();
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd")
                .create();
    }

    private void init(int port) {
        port(port);
        staticFiles.location("/assets");
        notFound((req, res) -> "Oooops: " + req.url());
        before((req, res) -> {
            System.out.printf("REQ: %s %s%n", req.requestMethod(), req.uri());
        });
    }

    private void routes() {
        path("/products", () -> {
            get("", (req, res) -> repo.getProducts(), gson::toJson);

            post("", JSON, (req, res) -> {
                var p = gson.fromJson(req.body(), Product.class);
                if (p == null) {
                    halt(400);
                }
                res.status(201);
                return repo.insert(p);
            }, gson::toJson);

            Function<Request, Product> lookup = (req) -> {
                var id = Integer.parseInt(req.params("id"));
                var p  = repo.getProductById(id);
                if (p.isEmpty()) {
                    halt(404);
                }
                return p.get();
            };

            get("/:id", (req, res) -> {
                var p = lookup.apply(req);
                return p;
            }, gson::toJson);

            put("/:id", JSON, (req, res) -> {
                var p = lookup.apply(req);
                var P = gson.fromJson(req.body(), Product.class);
                if (P == null) {
                    halt(400);
                }

                return p.update(P);
            }, gson::toJson);

            delete("/:id", (req, res) -> {
                var p = lookup.apply(req);
                repo.remove(p.getId());
                res.status(204);
                return null;
            }, gson::toJson);

        });
    }

}
