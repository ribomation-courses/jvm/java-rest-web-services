package ribomation.rest_ws.simple_jersey.domain;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BooksRepo {
    public static final BooksRepo       instance = new BooksRepo("/books.csv");
    private final       AtomicLong      nextId   = new AtomicLong(1);
    private             Map<Long, Book> repo;

    public BooksRepo() {
        repo = new TreeMap<>();
    }

    public BooksRepo(String resourcePath) {
        repo = load(resourcePath);
    }

    public List<Book> list() {
        return new ArrayList<>(repo.values());
    }

    public Book get(long id) {
        return repo.get(id);
    }

    public boolean delete(long id) {
        return repo.remove(id) != null;
    }

    public Book create(Book book) {
        book.setId(nextId.getAndIncrement());
        repo.put(book.getId(), book);
        return book;
    }

    public Book update(long id, Book delta) {
        Book book = get(id);
        if (book == null) return null;
        if (delta.getTitle() != null) book.setTitle(delta.getTitle());
        if (delta.getAuthor() != null) book.setAuthor(delta.getAuthor());
        if (delta.getPrice() > 0) book.setPrice(delta.getPrice());
        return book;
    }

    protected Map<Long, Book> load(String resourcePath) {
        InputStream is = getClass().getResourceAsStream(resourcePath);
        if (is == null) {
            throw new IllegalArgumentException("Cannot open resource " + resourcePath);
        }

        return new BufferedReader(new InputStreamReader(is))
                .lines()
                .skip(1)  //skip header
                .map(csv -> csv.split(";"))
                .map(fields -> {
                    long   id     = nextId.getAndIncrement();
                    String title  = fields[0];
                    String author = fields[1];
                    int    price  = Integer.parseInt(fields[2].trim());
                    return new Book(id, title, author, price);
                })
                .collect(Collectors.toMap(Book::getId, Function.identity()))
                ;
    }

    public void reset() { //used for unit tests
        nextId.set(1);
        repo = load("/books.csv");
    }
}
