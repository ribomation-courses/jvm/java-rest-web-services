Java for REST Web Services, 3 days
====

Welcome to this course.
The syllabus can be found at
[jvm/java-rest-web-services](https://www.ribomation.se/courses/jvm/java-rest-web-services)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code to demo projects


Installation Instructions
====

Java
----
The latest version of Java JDK installed. 
* [Java JDK Download](https://adoptopenjdk.net/)
  - N.B. This is not the ordinary Oracle download above, because starting with version 11, Oracle plan to charge for support of Java, which means you are better off going with OpenJDK in the first place. Go for the latest version and the HotSpot engine.



You also need a decent Java IDE, such as
* JetBrains IntellJ IDEA
    - https://www.jetbrains.com/idea/download
* MicroSoft Visual Code
    - https://code.visualstudio.com/



SDKMAN
----
We are going to use some JVM tools during the course. If you already have
GIT and its GIT-BASH window (or have access to BASH via *NIX); you can then install SDKMAN by the following command
inside a BASH window:

    curl -s "https://get.sdkman.io" | bash

Using the `sdk` command you can then install a java tool, such as gradle by:

    sdk install gradle

More info about SDKMAN
* [SDKMAN web](https://sdkman.io/)


Java Libraries
----
During the course we will use several Java libraries, such as Spark-Java,
DavidWebb, Resty, jJWT, that all can be downloaded as a dependency by a gradle build file.


HTTPie
----
HTTPie is a really good command-line tool for interacting with REST
web services. It's implemented in Python, so you need to install Python first.
* [HTTPie - Command line HTTP client](https://httpie.org/)
* [Python 3 - Required by HTTPie](https://www.python.org/downloads/release/python-363/)


GIT Repo
====

You need to have a GIT client installed to clone this repo. 
Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a `git clone` operation

    mkdir -p ~/java-course/my-solutions
    cd ~/java-course
    git clone https://gitlab.com/ribomation-courses/jvm/java-rest-web-services.git gitlab

Get the latest updates by a `git pull` operation

    cd ~/java-course/gitlab
    git pull

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
